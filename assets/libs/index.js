var swiper = new Swiper(".mySwiper", {
    direction: "vertical",
    mousewheel: {
        sensitivity: 2,
    }
});
var swiper2 = new Swiper(".mySwiper2", {
    slidesPerView: 1.5,
    navigation: {
        nextEl: '.swiper-next-el',
        prevEl: '.swiper-prev-el',
    },
    loop: false,
});
var swiper3 = new Swiper(".mySwiper3", {
    slidesPerView: 1.5,
    navigation: {
        nextEl: '.swiper-next-el-2',
        prevEl: '.swiper-prev-el-2',
    },
    loop: false,
});


let nextTo = document.querySelector('.swiper-section-nextSlide')
nextTo.addEventListener('click', function () {
    swiper.slideNext()
})